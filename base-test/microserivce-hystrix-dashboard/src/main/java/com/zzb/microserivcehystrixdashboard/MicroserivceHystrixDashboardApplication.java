package com.zzb.microserivcehystrixdashboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroserivceHystrixDashboardApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicroserivceHystrixDashboardApplication.class, args);
    }

}

