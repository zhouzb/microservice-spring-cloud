package com.zzb.configuration;

/**
 * 自定义负载均衡策略
 * 1.该类不能放在@SpringBootApplication，@ComponentScan的包或则子包中.如果该类中，
 * 则调用所有的微服务都会增加自定义的负载均衡策略
 * 2.如果要把该类放在@SpringBootApplication，@ComponentScan,需要添加一个注解，
 * 用于@ComponentScan过滤不需要加载的类
 */
//@Configuration
//// @RibbonClient(name = "foo", configuration = FooConfiguration.class)
//public class RibbonConfiguration {
//
//    @Autowired
//    private IClientConfig config;
//
//    /**
//     * 定义随机的负载均衡策略
//     *  ${@link org.springframework.cloud.netflix.ribbon.RibbonClientConfiguration}
//     * @param iClientConfig iClientConfig
//     * @return
//     */
//    @Bean
//    public IRule ribbonRule(IClientConfig iClientConfig) {
//        return new RandomRule();
//    }
//}