package com.zzb.basetestmovie.configuration;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import com.zzb.basetestmovie.annotaion.ExcludeRibbonComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhouzongbo on 2018/12/16 14:30
 */
@Configuration
@ExcludeRibbonComponentScan
public class RibbonConfiguration1 {

//    @Autowired
//    private IClientConfig config;

//    @Bean // 项目启动时已经注入了IClientConfig
//    public IRule ribbonRule(IClientConfig clientConfig) {
//        return new RandomRule();
//    }

    @Bean
    public IRule ribbonRule() {
        return new RandomRule();
    }
}
