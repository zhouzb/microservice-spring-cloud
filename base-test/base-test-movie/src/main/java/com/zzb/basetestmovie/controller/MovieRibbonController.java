package com.zzb.basetestmovie.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

/**
 * @author zhouzongbo on 2018/12/16 13:23
 */
@RestController
public class MovieRibbonController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private LoadBalancerClient loadBalancerClient;

    /**
     * 远程调用用户服务
     * @param id
     * @return
     */
    @GetMapping(value = "/movie/{id}")
    public Map<String,Object> getMovie(@PathVariable Long id) {
        // http://localhost:8080/getUser/
        // VIP : virtual IP 虚拟IP
        // VIP : 服务提供则的spring.application.name
        return this.restTemplate.getForObject("http://base-user-test:9090/getUser/"+id,Map.class);
    }

    @GetMapping(value = "/getInstance")
    public String getInstance() {
        // 选择要调用的微服务
        // 1.使用随机的负载均衡策略
        ServiceInstance choose = this.loadBalancerClient.choose("base-user-test");
        System.out.println("serviceId:"+choose.getServiceId()+"host:" + choose.getHost()+":port"+choose.getPort());

        // 2.使用默认的负载均衡策略
        ServiceInstance choose1 = this.loadBalancerClient.choose("base-user-test-provide");
        System.out.println("serviceId:"+choose1.getServiceId()+"host:" + choose1.getHost()+":port"+choose1.getPort());
        return "test";
    }
}
