package com.zzb.basetestmovie;

import com.zzb.basetestmovie.annotaion.ExcludeRibbonComponentScan;
import com.zzb.basetestmovie.configuration.RibbonConfiguration1;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
//使用自定义的负载均衡策略; user微服务的应用名： base-user-test
@RibbonClient(name = "base-user-test",configuration = RibbonConfiguration1.class)
@ComponentScan(excludeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION,classes = ExcludeRibbonComponentScan.class)})
public class BaseTestMovieApplication {

    /**
     * 增加负载均衡
     * RestTemplate加入Bean中
     * @return
     */
    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    public static void main(String[] args) {
        SpringApplication.run(BaseTestMovieApplication.class, args);
    }

}

