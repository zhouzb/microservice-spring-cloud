package com.zzb.basttesthystrix;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
//@EnableCircuitBreaker // 加入 Hystrix 注解
//@EnableHystrixDashboard // 加入Hystrix da
@EnableFeignClients(basePackages = "com.zzb") //加入feign
public class BastTestHystrixApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(BastTestHystrixApplication.class).run(args);
    }

}

