package com.zzb.basttesthystrix.client;

import com.zzb.basttesthystrix.client.impl.MovieFallFactory2;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 记录造成MovieClient造成回退的原因
 * @author zhouzongbo on 2018/12/23 14:25
 */
@FeignClient(name = "base-user-test-provide", fallbackFactory = MovieFallFactory2.class)
public interface MovieClient2 {

    @RequestMapping(value = "/getUser/fallback/{id}", method = RequestMethod.GET)
    String getUser(@PathVariable(value = "id") Long id);
}
