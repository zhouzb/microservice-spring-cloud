package com.zzb.basttesthystrix.client.impl;

import com.alibaba.fastjson.JSON;
import com.zzb.basttesthystrix.client.MovieClient2;
import com.zzb.basttesthystrix.domain.User;
import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author zhouzongbo on 2018/12/23 14:27
 */
@Component
public class MovieFallFactory2 implements FallbackFactory<MovieClient2> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieFallFactory2.class);
    @Override
    public MovieClient2 create(Throwable cause) {
        return id ->  {
            MovieFallFactory2.LOGGER.info("调用feign失败原因: {}", cause.getCause());
            User user = new User();
            user.setId(123L);
            return JSON.toJSONString(user);
        };
    }
}
