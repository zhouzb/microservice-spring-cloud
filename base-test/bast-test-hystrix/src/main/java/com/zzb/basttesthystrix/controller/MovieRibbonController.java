package com.zzb.basttesthystrix.controller;

import com.zzb.basttesthystrix.client.MovieClient2;
import com.zzb.basttesthystrix.client.MovieFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zhouzongbo on 2018/12/16 13:23
 */
@RestController
public class MovieRibbonController {

    @Autowired
    private MovieFeign movieFeign;

    @Autowired
    private MovieClient2 movieClient2;
    /**
     * 远程调用用户服务
     * @param id
     * @return
     */
    @GetMapping(value = "/movie/{id}")
    //@HystrixCommand(fallbackMethod = "defaultFallbackMethod")
    @SuppressWarnings("unchecked")
    public Map<String,Object> getMovie(@PathVariable Long id) {
        return movieFeign.getMovie(id);
    }

    public Map<String,Object> defaultFallbackMethod(Long id) {
        Map<String,Object> defaultMap = new HashMap<>();
        defaultMap.put("failure",id);
        return defaultMap;
    }

    @GetMapping("/movie/fallback/{id}")
    private String getUser(@PathVariable Long id) {
        return movieClient2.getUser(id);
    }
}
