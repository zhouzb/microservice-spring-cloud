package com.zzb.basttesthystrix.client.impl;

import com.zzb.basttesthystrix.client.MovieFeign;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * feign中的实现需要被定义为一个spring Bean
 * @author zhouzongbo on 2018/12/20 22:11
 */
@Component
public class MovieFeignImpl implements MovieFeign {
    @Override
    public Map<String, Object> getMovie(Long id) {
        Map<String,Object> defaultMap = new HashMap<>();
        defaultMap.put("hystrix","failure");
        return defaultMap;
    }
}
