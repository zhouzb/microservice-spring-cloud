package com.zzb.basttesthystrix.client;

import com.zzb.basttesthystrix.client.impl.MovieFeignImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

/**
 * 实现回退机制,动态调用
 * @author zhouzongbo on 2018/12/20 22:10
 */
@FeignClient(name = "base-user-test-provide", fallback = MovieFeignImpl.class)
public interface MovieFeign {

    @RequestMapping(value = "/getUser/{id}",method = RequestMethod.GET)
    Map<String,Object> getMovie(@PathVariable(value = "id") Long id);
}
