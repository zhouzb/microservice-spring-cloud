package com.zzb.basttesthystrix.domain;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable{
    /**
     * 和实体业务对应的用户实体
     */
    private static final long serialVersionUID = -7549254798751179167L;
    private Long id;
    /**用户名*/
    private String userName;

    /**密码*/
    private String password;

    /**年龄*/
    private String phone;

    private Date createDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}