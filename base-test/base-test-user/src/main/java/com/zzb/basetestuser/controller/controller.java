package com.zzb.basetestuser.controller;

import com.alibaba.fastjson.JSON;
import com.zzb.basetestuser.domain.User;
import com.zzb.basetestuser.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zhouzongbo on 2018/12/16 11:07
 */
@RestController
public class controller {
    @Autowired
    private UserService userService;

    @Value("${profile}")
    private String profile;

    @GetMapping("/getUser/{id}")
    public Map<String,Object> getUser(@PathVariable Long id) {
        User user = userService.getUser(id);
        Map<String,Object> resultMap = new HashMap<>();
        resultMap.put("id",user.getId());
        resultMap.put("userName",user.getUserName());
        resultMap.put("phone",user.getPhone());
        return resultMap;
    }

    @GetMapping("/getUser/fallback/{id}")
    public String getUserById(@PathVariable Long id) {
        User user = userService.getUser(id);
        return JSON.toJSONString(user);
    }
}
