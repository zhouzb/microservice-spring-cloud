package com.zzb.basetestuser.service.impl;

import com.zzb.basetestuser.dao.UserDao;
import com.zzb.basetestuser.domain.User;
import com.zzb.basetestuser.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author zhouzongbo on 2018/12/16 11:09
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public User getUser(Long id) {
        return userDao.getOne(id);
    }
}
