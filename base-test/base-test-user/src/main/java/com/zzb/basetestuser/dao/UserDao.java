package com.zzb.basetestuser.dao;

import com.zzb.basetestuser.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

/**
 * @author zhouzongbo on 2018/12/16 11:10
 */
@Repository
public interface UserDao extends JpaRepository<User,Long> {

}
