package com.zzb.basetestuser.service;

import com.zzb.basetestuser.domain.User;

/**
 * @author zhouzongbo on 2018/12/16 11:09
 */
public interface UserService {

    User getUser(Long id);
}
