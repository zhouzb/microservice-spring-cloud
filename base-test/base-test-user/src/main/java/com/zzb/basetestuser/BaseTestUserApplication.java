package com.zzb.basetestuser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableEurekaClient
@ComponentScan(basePackages = "com.zzb")
public class BaseTestUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(BaseTestUserApplication.class, args);
    }

}

