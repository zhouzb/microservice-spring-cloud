package com.zzb.microserivcezuulupload.controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * 通过Zuul进行文件上传
 * @author zhouzongbo on 2018/12/23 21:09
 */
@RestController
public class ZuulFileUpload {


    @GetMapping("/fileUploadPage")
    public ModelAndView getFileUploadPage() {
        return new ModelAndView("fileupload");
    }

    @RequestMapping(value = "/fileUpload", method = RequestMethod.POST)
    public void saveFileUpload(HttpServletRequest request, @RequestParam(name = "file") MultipartFile file) throws IOException {

        String originalFilename = file.getOriginalFilename();


        InputStream inputStream = file.getInputStream();

        FileOutputStream fos = new FileOutputStream("D:\\uploadFile" +File.separator + originalFilename);
        fos.write(inputStream.read());

        inputStream.close();
        fos.close();
        fos.flush();

    }
}
