package com.zzb.microserivcezuulupload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class MicroserivceZuulUploadApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicroserivceZuulUploadApplication.class, args);
    }

}

