package com.zzb.basttestfeigncustom.feign;


import com.zzb.configuration.FeignClientConfiguration;
import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;

import java.util.Map;

/**
 * Feign通过将注释处理成模板化的请求来工作。参数在输出之前以一种简单的方式应用于这些模板。
 * 虽然Feign仅限于支持基于文本的api，但它极大地简化了系统方面的工作，比如重放请求。
 * 此外，Feign使您的转换单元测试更加容易
 * @author zhouzongbo on 2018/12/17 21:52
 */
@FeignClient(name = "base-user-test-provide", configuration = FeignClientConfiguration.class)
public interface MovieFeignClient {

    /**
     * 使用feign默认的注解, GET地址为服务提供者地址
     * @param id id
     * @return map
     */
    @RequestLine("GET /getUser/{id}")
    Map<String,Object> getUser(@Param("id") Long id);
}
