package com.zzb.basttestfeigncustom.controller;

import com.zzb.basttestfeigncustom.feign.FeignClient2;
import com.zzb.basttestfeigncustom.feign.MovieFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author zhouzongbo on 2018/12/16 13:23
 */
@RestController
public class MovieRibbonController {

    @Autowired
    private MovieFeignClient feignClient;

    @Autowired
    private FeignClient2 feignClient2;

    /**
     * 远程调用用户服务
     * @param id
     * @return
     */
    @GetMapping(value = "/movie/{id}")
    public Map<String,Object> getMovie(@PathVariable Long id) {
        // http://localhost:8080/getUser/
        // VIP : virtual IP 虚拟IP
        // VIP : 服务提供则的spring.application.name
        return this.feignClient.getUser(id);
    }

    /**
     * http://192.168.31.228:9094/base-user-test-provide
     * @param serviceName serviceName
     * @return
     */
    @GetMapping("/{serviceName}")
    public String getServiceName(@PathVariable String serviceName) {
        return feignClient2.getServiceInfoFromEurekaByServiceName(serviceName);
    }
}
