package com.zzb.basttestfeigncustom.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 测试{@link com.zzb.configuration.FeignClientConfiguration} 配置类只针对使用则
 * 新版本name必须和url连用
 * @author zhouzongbo on 2018/12/17 22:26
 */
@FeignClient(name = "eureka", url = "http://localhost:8080/")
public interface FeignClient2 {

    @RequestMapping(value = "/eureka/apps/{serviceName}", method = RequestMethod.GET)
    String getServiceInfoFromEurekaByServiceName(@PathVariable(value = "serviceName") String serviceName);
}
