package com.zzb.configuration;

import feign.Contract;
import feign.auth.BasicAuthRequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhouzongbo on 2018/12/17 22:03
 */
@Configuration
public class FeignClientConfiguration {
    /**
     * 注意: 如果此处使用的Feign的契约，则不能使用springmvc的注解,如@requestMaping
     * 如果不修改配置文件，则默认使用SpringMvcContract
     * @return
     */
    @Bean
    public Contract feignContract() {
        return new feign.Contract.Default();
    }

    /**
     * 如果eureka配置了加密，则需要加上该Bean
     * @return
     */
    @Bean
    public BasicAuthRequestInterceptor basicAuthRequestInterceptor() {
        return new BasicAuthRequestInterceptor("user", "password");
    }
}
