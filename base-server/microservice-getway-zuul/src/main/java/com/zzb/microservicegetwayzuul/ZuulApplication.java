package com.zzb.microservicegetwayzuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * 每个版本的配置文件,通过.bak1-n 来表示
 * 其他服务,通过http://localhost:8081/{zuul.routes.具体的服务名}  访问
 */
@SpringBootApplication
@EnableZuulProxy
public class ZuulApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZuulApplication.class, args);
    }

}

