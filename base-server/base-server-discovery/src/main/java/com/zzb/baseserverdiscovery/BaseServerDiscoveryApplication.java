package com.zzb.baseserverdiscovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
//@EnableDiscoveryClient
@EnableEurekaClient
public class BaseServerDiscoveryApplication {

    public static void main(String[] args) {
        SpringApplication.run(BaseServerDiscoveryApplication.class, args);
    }

}

