package com.zzb.baseservereureka;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * Stand up a Eureka Service Registry
 */
@SpringBootApplication
@EnableEurekaServer
public class BaseServerEurekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(BaseServerEurekaApplication.class, args);
    }


    @Autowired
    private EurekaClient eurekaClient;

    public  String getServiceUrl() {
        InstanceInfo nextServerFromEureka = eurekaClient.getNextServerFromEureka("baseserverdiscovery", false);
        return nextServerFromEureka.getHomePageUrl();
    }
}

